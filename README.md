# Plant seedlings

A training wheel: Deployment of a continuous regression model for the [V2 Plant Seedlings dataset](https://www.kaggle.com/datasets/vbookshelf/v2-plant-seedlings-dataset).

* [Building the model in a CNN pipeline](notebooks)
* [Production model](production-model)
* [Packaging](production-model-package)
* [Adding to API](api)

## Resources

### AWS
* [AWS S3](https://aws.amazon.com/s3/)

### API
* [File Uploads in Flask](http://flask.pocoo.org/docs/1.0/patterns/fileuploads/)

### CI and publishing
* [Generating artifacts in the CI to persist the trained model between jobs](https://circleci.com/docs/2.0/artifacts/)
* [How to increase memory](https://discuss.circleci.com/t/increase-4g-memory-limit/9070/5)
* [Using GPUs on Circle CI](https://circleci.com/docs/2.0/gpu/)
* [Alternative resource classes](https://circleci.com/docs/2.0/configuration-reference/#resource_class)
* [Running Keras models on a GPU](https://keras.io/getting-started/faq/#how-can-i-run-keras-on-gpu)
* [Loading Keras datasets that do not fit in memory](https://keras.io/getting-started/faq/#how-can-i-use-keras-with-datasets-that-dont-fit-in-memory)

