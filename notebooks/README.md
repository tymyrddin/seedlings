# Seedlings R&D CNN pipeline

![To be done](../assets/to-be-done.png)

- Exploratory Data Analysis (EDA)
- Feature Engineering
- Feature Selection
- Model Training
- Obtaining Predictions / Scoring
- Feature engineering with Open Source