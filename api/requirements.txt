uvicorn>=0.16.0,<0.18.0
fastapi>=0.78.0,<1.0.0
python-multipart>=0.0.5,<0.1.0
pydantic>=1.9.1,<1.10.0
typing_extensions>=4.2.0,<5.0.0
loguru==0.6.0
# ugh
# tid-regression-model